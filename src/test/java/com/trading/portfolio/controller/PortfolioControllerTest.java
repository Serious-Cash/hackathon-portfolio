package com.trading.portfolio.controller;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class PortfolioControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void tradingControllerFindAllSuccess() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/portfolio"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void tradingControllerFindAllFailures() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/trad"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }
}