package com.trading.portfolio.entity;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

/**
 * Trading test class testing each set field, ensuring getters and setters are functioning
 * @author Lyn, Aaron, Michelle
 * @version 1.0
 * @since 2021-10-08
 */
@SpringBootTest
public class PortfolioItemTest {
    private PortfolioItem portfolio;
    private static final String testStockTicker = "TestString";
    private static final int testVolume = 3;


    @BeforeEach
    public void setup() {
        this.portfolio = new PortfolioItem();
    }

    @Test
    public void setTestStockTicker() {
        this.portfolio.setStockTicker(testStockTicker);
        assertEquals(testStockTicker, this.portfolio.getStockTicker());
    }

    @Test
    public void setTestVolume() {
        this.portfolio.setVolume(testVolume);
        assertEquals(testVolume, this.portfolio.getVolume());
    }

}