package com.trading.portfolio.service;

import com.trading.portfolio.entity.PortfolioItem;
import com.trading.portfolio.repository.PortfolioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PortfolioService {

    @Autowired
    PortfolioRepository portfolioRepository;

    public List<PortfolioItem> findAll() {

        return portfolioRepository.findAll();
    }


    public PortfolioItem save(PortfolioItem trading) {

        return portfolioRepository.save(trading);
    }

    public PortfolioItem update(PortfolioItem item) {
        portfolioRepository.findById(item.getId()).get();

        return portfolioRepository.save(item);
    }

    public void delete(long id) {
        portfolioRepository.deleteById(id);
    }


    public PortfolioItem findById(long id){

        return portfolioRepository.findById(id).get();
    }
}
