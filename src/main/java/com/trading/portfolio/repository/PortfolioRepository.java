package com.trading.portfolio.repository;
import com.trading.portfolio.entity.PortfolioItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface PortfolioRepository extends JpaRepository<PortfolioItem, Long> {
}
