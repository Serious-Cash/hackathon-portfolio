package com.trading.portfolio.entity;

import javax.persistence.*;

@Entity
public class PortfolioItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "itemid")
    private Long id;

    @Column(name = "stockticker")
    private String stockticker;

    @Column(name = "volume")
    private int volume;

    public PortfolioItem() {

    }

    public PortfolioItem(Long id, String stockticker, int volume) {
        this.id = id;
        this.stockticker = stockticker;
        this.volume = volume;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStockTicker() {
        return stockticker;
    }

    public void setStockTicker(String stockticker) {
        this.stockticker = stockticker;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
