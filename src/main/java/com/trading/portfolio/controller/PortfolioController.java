package com.trading.portfolio.controller;

import com.trading.portfolio.entity.PortfolioItem;
import com.trading.portfolio.service.PortfolioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/portfolio")
public class PortfolioController {

    @Autowired
    private PortfolioService portfolioService;

    @GetMapping
    public List<PortfolioItem> findAll() {
        return this.portfolioService.findAll();
    }

    @PostMapping
    public ResponseEntity<PortfolioItem> save(@RequestBody PortfolioItem item) {
        try {
            return new ResponseEntity<PortfolioItem>(portfolioService.save(item), HttpStatus.CREATED);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        try {
            portfolioService.delete(id);
        } catch (EmptyResultDataAccessException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping
    public ResponseEntity<PortfolioItem> update(@RequestBody PortfolioItem item) {
        try {
            return new ResponseEntity<PortfolioItem>(portfolioService.update(item), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            //LOG.debug("update for unknown id: [" + trading + "]");
            return new ResponseEntity<PortfolioItem>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<PortfolioItem> findById(@PathVariable long id) {
        try {
            return new ResponseEntity<PortfolioItem>(portfolioService.findById(id), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        }
    }
}
