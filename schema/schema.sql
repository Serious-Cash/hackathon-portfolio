CREATE Schema portfolio;

USE portfolio;

CREATE TABLE IF NOT EXISTS items
(itemid INT NOT NULL AUTO_INCREMENT,
 stock_ticker VARCHAR(24) NULL,
    volume int NULL,
    PRIMARY KEY(itemid));
